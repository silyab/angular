# Run

# Install

- node 6+ et npm 3+

```
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get install -y nodejs
```

- Angular cli
```
npm install -g @angular/cli
```

- Dépendances
```
cd trainer
npm install
```

- Update Angular Cli

https://github.com/angular/angular-cli#updating-angular-cli