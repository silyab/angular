import { Proto1Page } from './app.po';

describe('proto1 App', function() {
  let page: Proto1Page;

  beforeEach(() => {
    page = new Proto1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
