import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css', '../../styles/Xlarge.css', '../../styles/large.css',
    '../../styles/medium.css', '../../styles/tablet.css', '../../styles/phone.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
