import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-switcher',
  templateUrl: './switcher.component.html',
  styleUrls: ['./switcher.component.css']
})
export class SwitcherComponent implements OnInit {

  erreur = false;

  constructor(private router: Router) {
  }

  ngOnInit() {
    try {
      let switch_i = localStorage.getItem('switch');
      let i;

      if (switch_i === null) {
        i = Math.random();
      }
      else {
        i = parseInt(switch_i)
      }

      if (i <= 1) {
        localStorage.setItem('switch', '0');
        this.router.navigateByUrl('/conseil');
      }
      else {
        localStorage.setItem('switch', '1');
        this.router.navigateByUrl('/bijoux');
      }
    }
    catch (e){
      this.erreur = true;
      console.log(e);
    }
  }

}
