import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {MnFullpageService, MnFullpageOptions} from "ng2-fullpage";

import {AuthService} from "../../services/auth.service";
import {DataService} from "../../services/data.service";
import {StyleComponent} from "../style/style.component";
import {ShopComponent} from "../shop/shop.component";
import {formData} from "./form";
import {animations} from "../../styles/animations";
import {environment} from "../../../environments/environment";


@Component({
  selector: 'app-navbar',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../../styles/Xlarge.css', '../../styles/large.css',
              '../../styles/medium.css', '../../styles/tablet.css', '../../styles/phone.css'],
  animations: animations
})
export class HomeComponent implements OnInit {

  id;
  questions = formData;
  pages = {page0:[],page1:[],page2:[],page3:[],page4:[],page5_1:[],page5_2:[],page7:[]};
  pages_done = {};
  conf = {};
  conf_changed = true;
  progress = 5;
  show_progress = false;
  fatal_error = false;

  constructor(private fullpageService: MnFullpageService, private auth: AuthService, private dataService: DataService) {
  }

  @Input() public options:MnFullpageOptions = new MnFullpageOptions({
    afterLoad: click,
    normalScrollElements: '.fp-section',
  });

  @ViewChild(StyleComponent) styleComponent:StyleComponent;
  @ViewChild(ShopComponent) shopComponent:ShopComponent;

  ngOnInit() {
    localStorage.setItem('switch', '0');
    this.getMyId();
    this.getConf();

    // Bloque le scrolling avec souris
    let _this = this;
    setTimeout(function(){
      _this.fullpageService.setAllowScrolling(false);}, 100);
  }

  getMyId(){
    this.id = localStorage.getItem('clientID');

    /* Nouvel id si pas d'id*/
    if (this.id === null){
      this.askNewId();
    }
    /* Test si l'id toujours valable */
    else {
      this.dataService.testId(this.id).subscribe((response) => {
        if (response.hasOwnProperty('error')) {
          /* Si not found on demande un nouvel id */
          if (response.error === 'not found'){
            console.log('Id plus valable demande d\'un nouveau...');
            this.askNewId();
          }
          /* autre erreur */
          else {
            console.log('Le serveur a rencontré une erreur avec mon id');
            this.monitorError();
          }
        }
      }, (error) => {
        console.log('Impossible de contacter le serveur : ' + error);
          this.monitorError();
      });
    }
  }

  askNewId(){
    this.dataService.getId().subscribe((response) => {
      this.id = response;
      this.dataService.createUser(this.id).subscribe((response) => {
        if(response.hasOwnProperty('error')){
          console.log('Impossible de créer un nouvel utilisateur, error : ', response['error']);
          this.monitorError();
        }
        else{
          localStorage.setItem('clientID', this.id);
        }
      }, (error) => {
        console.log('Impossible de créer un nouvel utilisateur, error : ', error);
        this.monitorError();
      });
    }, (error) => {
      console.log('Impossible de créer un nouvel utilisateur, error : ', error);
      this.monitorError();
    });
  }

  getConf(){
    this.conf = JSON.parse(localStorage.getItem('conf'));

    if(this.conf === null) this.conf = {};

    // Valeurs par défault
    if(!("taille" in this.conf)) this.conf['taille'] = 'moyenne';
    if(!("peau" in this.conf)) this.conf['peau'] = 'vanille';
    if(!("yeux" in this.conf)) this.conf['yeux'] = 'brun';
    if(!("visage" in this.conf)) this.conf['visage'] = 'ovale';
    if(!("cou" in this.conf)) this.conf['cou'] = 'moyen';
    if(!("main" in this.conf)) this.conf['main'] = 'moyen';

    if(!("type" in this.conf)) this.conf['type'] = 'Bague';
    if(!("mode" in this.conf)) this.conf['mode'] = 1;
    if(!("filtre_prix" in this.conf)) this.conf['filtre_prix'] = {'0':'','1':''};

    if(!("col" in this.conf)) this.conf['col'] = 'rond';
    if(!("occasion" in this.conf)) this.conf['occasion'] = 'jour';
    if(!("occasion_tenue" in this.conf)) this.conf['occasion_tenue'] = 'jour';
    if(!("couleur_tenue" in this.conf)) this.conf['couleur_tenue'] = 'rose';

    // Initialisation des formulaires
    for (let key in this.conf) {
      if (this.conf.hasOwnProperty(key)) {
        if (this.questions.hasOwnProperty(key)) {
          for(let i = 0; i < this.questions[key].length; ++i) {
            if (this.questions[key][i].value != this.conf[key])
              continue;
            this.questions[key][i]['checked'] = "checked";
            break;
          }
        }
      }
    }
  }

  // Maj de conf
  saveChangeConf(key, value){
    this.conf_changed = true;
    this.conf[key] = value;

    if (key === 'mode' && value === 1){
      this.pages_done['5_1'] = false;
    }
    if (key === 'mode' && value === 2){
      this.pages_done['5_2'] = false;
    }
  }

  // Sauvegarde local et online de conf
  saveConf(page_num, call_best=false){
    if (this.conf_changed) {
      this.conf_changed = false;
      this.conf['page_num'] = page_num;
      localStorage.setItem('conf', JSON.stringify(this.conf));

      this.dataService.setConf(this.id, this.conf).subscribe((response) => {
        if(call_best){
          this.shopComponent.getBest();
        }
      }, (error) => {
        console.log('Impossible d\'envoyer conf. error : ' + error);
      });
    }
    else if (call_best){
      this.shopComponent.getBest();
    }
  }

  // controle de la barre de progression du menu
  setProgress(i){
    switch(i){
      case 0:
        this.progress = 8;
        this.show_progress = false;
        break;
      case 1:
        this.progress = 8;
        this.show_progress = true;
        break;
      case 2:
        this.progress = 33;
        this.show_progress = true;
        break;
      case 3:
        this.progress = 50;
        this.show_progress = true;
        break;
      case 4:
        this.progress = 63;
        this.show_progress = true;
        break;
      case 5:
        this.progress = 77;
        this.show_progress = true;
        break;
      case 6:
        this.progress = 100;
        this.show_progress = true;
        break;
    }
  }

  // Activation spéciale pour chaque page
  type(page_num){

    let phrase;
    let timing;

    if(this.pages_done[page_num]) {
      return
    }

    this.pages_done[page_num]=true;

    switch (page_num) {
      case 0:
        phrase = "Salut, je suis Silya !";
        timing = {6:800};
        this.typing(phrase, timing, "page0");

        break;
      case 1:
        phrase = "Super ! Avant de commencer voici quelques indications";
        timing = {8:800};
        this.typing(phrase, timing, "page1");

        break;
      case 2:
        phrase = "C'est parti ! Commençons par la partie morphologie";
        timing = {14:800};

        this.typing(phrase, timing, "page2");

        break;
      case 3:
        phrase = "J'ai encore besoin de savoir deux trois choses pour te conseiller au mieux...";
        timing = {};

        this.typing(phrase, timing, "page3");

        break;
      case 4:
        phrase = "Quel type de bijoux recherches-tu ?";
        timing = {};

        this.typing(phrase, timing, "page4");

        break;
      case '5_1':
        phrase = "D'accord, dis m'en plus sur cette occasion...";
        timing = {9:800};

        this.typing(phrase, timing, "page5_1");

        break;

      case '5_2':

        phrase = "D'accord, dis m'en plus sur ta tenue...";
        timing = {9:800};

        this.typing(phrase, timing, "page5_2");

        break;

      case 6:

        if (environment.production) {
          eventFire(document.getElementById('pixel'), 'click');
        }
        eventFire(document.getElementById('survey'), 'click');

        break;
    }
  }

  // Ecriture des phrases Silya
  typing(phrase, timing, page) {
    let w = 0;
    let _this = this;

    this.pages[page] = [];

    for (let i=0;i<phrase.length;i++) {
      w += (timing[i] === undefined ? 0 : timing[i]);
      (function(i, w) {
        setTimeout(function(){
          _this.pages[page].push(phrase.charAt(i));}, 25*i + w);
      })(i, w);
    }
  }

  // Change la couleur de tenue_couleur
  changeTenue(value){
    for (let color of formData.couleur_tenue){
      if (color.value === value){
        document.getElementById('img-cache').style.backgroundColor = color.hexa;
        break;
      }
    }
  }

  // Initialise la couleur de tenue_couleur
  init_color(){
    // Recherche de l'hexa correspondant
    for(let i = 0; i < this.questions['couleur_tenue'].length; ++i) {
      if(this.questions['couleur_tenue'][i].value != this.conf['couleur_tenue'])
        continue;
      this.changeTenue(this.questions['couleur_tenue'][i]['value']);
      break;
    }
  }

  goTopShop(){
    eventFire(document.getElementById('scroll2'), 'click');
  }

  monitorError(){
    if (environment.production) {
      eventFire(document.getElementById('pixel3'), 'click');
      eventFire(document.getElementById('google'), 'click');
    }
    this.fatal_error = true;
  }
}

// Utile pour déclancher type()
function click(_, index){
  eventFire(document.getElementById('show' + (index-1)), 'click');
}

function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    let evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}
