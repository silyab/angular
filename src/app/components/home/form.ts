export let formData = {
    taille:[
      {name:"Grande", pic:"icon_taille_grande", id:"taille_g", value:"grande", tooltip:"plus d'1m75"},
      {name:"Moyenne", pic:"icon_taille_moyenne", id:"taille_m", value:"moyenne", tooltip:"entre 1m60 et 1m75"},
      {name:"Petite", pic:"icon_taille_petite", id:"taille_p", value:"petite", tooltip:"moins d'1m60"}],
    peau:[
      {name:"Foncée", pic:"icon_peau_chocolat", id:"peau_ch", value:"chocolat"},
      {name:"Mate", pic:"icon_peau_caramel", id:"peau_c", value:"caramel"},
      {name:"Claire", pic:"icon_peau_vanille", id:"peau_v", value:"vanille"}],
    yeux:[
      {name:"Bleus", pic:"icon_oeil_bleu", id:"yeux_b", value:"bleu"},
      {name:"Verts", pic:"icon_oeil_vert", id:"yeux_v", value:"vert"},
      {name:"Noisette", pic:"icon_oeil_noisette", id:"yeux_n", value:"noisette"},
      {name:"Bruns", pic:"icon_oeil_brun", id:"yeux_br", value:"brun"}],
    visage:[
      {name:"Rond", pic:"icon_visage_rond", id:"visage_r", value:"rond",
      tooltip_pic:"tooltip_rond", tooltip:"Comme Ginnifer Goodwin <br> (Visage rond)"},
      {name:"Ovale", pic:"icon_visage_ovale", id:"visage_o", value:"ovale",
        tooltip_pic:"tooltip_ovale", tooltip:"Comme Jessica Biel <br> (Bas du visage rond)"},
      {name:"Carré", pic:"icon_visage_carre", id:"visage_c", value:"carre",
        tooltip_pic:"tooltip_carre", tooltip:"Comme Keira Knightley <br> (Bas du visage carré)"},
      {name:"Coeur", pic:"icon_visage_coeur", id:"visage_co", value:"coeur",
        tooltip_pic:"tooltip_coeur", tooltip:"Comme Reese Witherspoon <br> (Bas du visage pointu)"}],
    cou:[
      {name:"Long", pic:"icon_cou_long", id:"cou_l", value:"long"},
      {name:"Moyen", pic:"icon_cou_moyen", id:"cou_m", value:"moyen"},
      {name:"Petit", pic:"icon_cou_petit", id:"cou_p", value:"petit"}],
    main:[
      {name:"Longs", pic:"icon_main_long", id:"main_l", value:"long"},
      {name:"Moyens", pic:"icon_main_moyen", id:"main_m", value:"moyen"},
      {name:"Petits", pic:"icon_main_petit", id:"main_p", value:"petit"}],
    col:[
      {name:"Plutôt rond", pic:"icon_col_rond", id:"col_r", value:"rond"},
      {name:"En V ou carré", pic:"icon_col_v", id:"col_v", value:"v"},
      {name:"Jusqu'au cou", pic:"icon_col_cou", id:"col_c", value:"cou"},
      {name:"Bustier", pic:"icon_col_bustier", id:"col_b", value:"bustier"}],
    type:[
      {name:"Bagues", pic:"icon_type_bague", id:"type_b", value:"Bague"},
      {name:"Colliers", pic:"icon_type_collier", id:"type_c", value:"Collier"},
      {name:"Boucles d'oreilles", pic:"icon_type_boucle", id:"type_bo", value:"Boucle_oreil"},
      {name:"Bracelets", pic:"icon_type_bracelet", id:"type_br", value:"Bracelet"},
      {name:"Montres", pic:"icon_type_montre", id:"type_m", value:"Montre", disabled:"disabled"},],
    occasion:[
      {name:"Le travail (bijou discret)", pic:"icon_occasion_travail", id:"occas_t", value:"travail"},
      {name:"Toutes occasions", pic:"icon_occasion_jour", id:"occas_j", value:"jour"},
      {name:"Une occasion spéciale", pic:"icon_occasion_soiree", id:"occas_s", value:"soiree"}],
    couleur_tenue:[
      {name:"Noir", hexa:"#000", value:"noir"},
      {name:"Blanc", hexa:"#F4F4F4", value:"blanc"},
      {name:"Gris", hexa:"#B2B2B2", value:"gris"},
      {name:"Beige", hexa:"#F4D7C3 ", value:"beige"},
      {name:"Marron", hexa:"#522D27", value:"marron"},
      {name:"Marron Clair", hexa:"#AB7954", value:"marr_clair"},
      {name:"Rouge", hexa:"#E20E23", value:"rouge"},
      {name:"Rouge Bordeau", hexa:"#AA1B29", value:"bordeau"},
      {name:"Rose", hexa:"#e84763", value:"rose"},
      {name:"Rose Clair", hexa:"#F8D8DD", value:"rose_clair"},
      {name:"Bleu", hexa:"#2D49AD", value:"bleu"},
      {name:"Bleu Clair", hexa:"#94E1F3", value:"bleu_clair"},
      {name:"Bleu-Vert", hexa:"#25596E", value:"bleu_vert"},
      {name:"Violet", hexa:"#623D8B", value:"violet"},
      {name:"Vert", hexa:"#006F44", value:"vert"},
      {name:"Orange", hexa:"#FF8B02", value:"orange"},
      {name:"Jaune", hexa:"#FFDC0E", value:"jaune"}],
    occasion_tenue:[
      {name:"Le travail (bijou discret)", pic:"icon_occasion_travail", id:"occas2_t", value:"travail"},
      {name:"Toutes occasions", pic:"icon_occasion_jour", id:"occas2_j", value:"jour"},
      {name:"Une occasion spéciale", pic:"icon_occasion_soiree", id:"occas2_s", value:"soiree"}],
  };

