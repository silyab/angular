import {Component, OnInit, HostListener, Input} from '@angular/core';
import {DataService} from "../../services/data.service";
import {animations} from "../../styles/animations";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css', '../../styles/Xlarge.css', '../../styles/large.css',
    '../../styles/medium.css', '../../styles/tablet.css', '../../styles/phone.css'],
  animations: animations
})
export class ShopComponent implements OnInit {

  @Input('parent') parent;
  res = {'bijoux': [], 'completion': 0};
  current_bijou;
  show_popup = false;
  show_popupin = false;
  popup_page = 1;
  show_popup2 = false;
  jaimepas_show = false;
  key_color = ['couleur_metal', 'couleur_pierre', 'couleur_principal'];
  ratio = 0.8;
  nbr_case = 1;
  max_bijou = 20;
  loading = true;
  first_time = true;

  ratio_popin_top = 0;
  ratio_popin_left = 0;
  popin_top = '0';
  popin_left = '0';
  popin_fleche_top = '0';
  popin_fleche_left = '0';
  popin2_top = '0';
  popin2_left = '0';
  popin2_fleche_top = '0';
  popin2_fleche_left = '0';

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.resize(window.innerWidth);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resize(event.target.innerWidth);
  }

  resize(width) {

    if (width >= 1500) {
      this.ratio = 0.6;
      this.ratio_popin_top = 100;
      this.ratio_popin_left = 100;
    }
    else if (width >= 1200) {
      this.ratio = 0.45;
      this.ratio_popin_top = 50;
      this.ratio_popin_left = 90;
    }
    else if (width >= 992) {
      this.ratio = 0.45;
      this.ratio_popin_top = 50;
      this.ratio_popin_left = 90;
    }
    else if (width >= 768) {
      this.ratio = 0.45;
      this.ratio_popin_top = 50;
      this.ratio_popin_left = 90;
    }
    else {
      this.ratio = 0.45;
      this.ratio_popin_top = 50;
      this.ratio_popin_left = 90;
    }


    if (width >= 2134) {
      this.nbr_case = 6;
    }
    else if (width >= 1789) {
      this.nbr_case = 5;
    }
    else if (width >= 1214) {
      this.nbr_case = 4;
    }
    else if (width >= 927) {
      this.nbr_case = 3;
    }
    else if (width >= 640) {
      this.nbr_case = 2;
    }
    else {
      this.nbr_case = 1;
    }
  }

  open_popupin(){
    let _this = this;
    setTimeout(function(){

      _this.show_popupin = true;
      let rect = document.getElementById('shop-card-0').getBoundingClientRect();
      let rect2 = document.getElementById('barre').getBoundingClientRect();

      _this.popin_top = rect.top+_this.ratio_popin_top + 'px';
      _this.popin_left = rect.left+_this.ratio_popin_left + 90 + 'px';
      _this.popin_fleche_top = rect.top+_this.ratio_popin_top + 'px';
      _this.popin_fleche_left = rect.left+_this.ratio_popin_left + 'px';

      _this.popin2_top = rect2.top - 90 + 'px';
      _this.popin2_left = rect2.left + 90 + 'px';
      _this.popin2_fleche_top = rect2.top - 90 + 'px';
      _this.popin2_fleche_left = rect2.left + 'px';

    }, 500);
  }

  openCard(bijou){
    bijou.closing = false;
    bijou.hover = true;
    setTimeout(function () {
      if (!bijou.closing) {
        bijou.hover2 = true;
      }
    }, 200);
  }

  closeCard(bijou){
    bijou.closing = true;
    bijou.hover2 = false;
    setTimeout(function () {
      if (bijou.closing) {
        bijou.hover = false;
      }
    }, 200);
  }

  isXl(bijou){
    let total_length = bijou.score_data.phrases_morpho.length + bijou.score_data.phrases_occasion.length;
    return (this.nbr_case > 4 && total_length > 4) || (this.nbr_case <= 4 && total_length > 3);
  }

  isLast(bijou){
    return ((bijou.rank+1) % this.nbr_case == 0);
  }

  getBest(go_up=true) {

    eventFire(document.getElementById('pixel'), 'click');
    if (go_up){
      this.parent.goTopShop();
    }
    this.loading = true;
    this.dataService.getBest(this.parent.id, this.max_bijou).subscribe((response) => {
      if (response.hasOwnProperty('error')) {
        console.log('Impossible de récupérer la sélection. error : ', response['error']);
        this.loading = false;
      }
      else {
        this.res = response;
        this.loading = false;
        if (this.first_time){
          this.open_popupin();
          this.first_time = false;
        }
      }
    }, (error) => {
      console.log('Impossible de récupérer les best bijoux. error : ' + error);
      this.loading = false;
    });
  }

  resetPref(){
    this.dataService.resetPref(this.parent.id).subscribe((response) => {
      this.getBest();
    }, (error) => {
      console.log('Impossible de reset prefs. error : ' + error);
    });
  }

  monitorBijou(bijou_id){
    if (environment.production) {
      eventFire(document.getElementById('pixel2'), 'click');
      eventFire(document.getElementById('google2'), 'click');
    }
    this.dataService.monitorBijou(this.parent.id, 0, bijou_id).subscribe((response) => {
    }, (error) => {
      console.log('Impossible de log bijou. error : ' + error);
    });
  }

}

function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    let evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}
