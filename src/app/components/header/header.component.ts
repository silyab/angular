import {Component, OnInit, Input} from '@angular/core';
import {animations} from "../../styles/animations";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css', '../../styles/Xlarge.css', '../../styles/large.css',
    '../../styles/medium.css', '../../styles/tablet.css', '../../styles/phone.css'],
  animations: animations
})
export class HeaderComponent implements OnInit {

  @Input('parent') parent;
  _Math = Math;

  constructor() { }

  ngOnInit() {
  }

}
