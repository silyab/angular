import {
  Component, OnInit, style, HostListener, Input
} from '@angular/core';
import {DataService} from "../../services/data.service";
import {animations} from "../../styles/animations";

@Component({
  selector: 'app-style',
  templateUrl: './style.component.html',
  styleUrls: ['./style.component.css', '../../styles/Xlarge.css', '../../styles/large.css',
    '../../styles/medium.css', '../../styles/tablet.css', '../../styles/phone.css'],
  animations: animations
})
export class StyleComponent implements OnInit {

  @Input('parent') parent;
  @Input('user_id') user_id;
  @Input('dict') dict;
  @Input('phrases') phrases;
  @Input('bijou') bijou;
  key_color = ['couleur_metal', 'couleur_pierre', 'couleur_principal'];
  jaimepas_dict = {};
  poids_neg = 3;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
  }

  sendData(ordre){

    // Préparation de la requete
    let dict = null;

    // ordre : jaime
    if (ordre === 'jaime'){

      // Toutes les carac
      dict = this.dict;
      dict['positif'] = true;
      dict['type'] = this.parent.parent.conf.type;

    }

    // ordre : jaimepas
    else if (ordre === 'jaimepas'){
      dict = {};
      dict['positif'] = false;
      dict['id'] = this.dict.id;
      dict['type'] = this.parent.parent.conf.type;
      dict['dic'] = {'style': {value: [], poids: this.poids_neg},
                      'symbolique': {value: [], poids: this.poids_neg}};

      // Ajout taille et couleurs
      for (let key of this.key_color.concat(['taille'])){
        if (this.jaimepas_dict.hasOwnProperty('normale') && this.jaimepas_dict['normale'].hasOwnProperty(key)) {
          dict['dic'][key] = this.dict.dic[key];
          dict['dic'][key]['poids'] = this.poids_neg;
        }
      }

      // Ajout styles et symbolique
      for (let key of ['style', 'symbolique']){
        if (this.jaimepas_dict.hasOwnProperty(key)){
          for (let elm in this.jaimepas_dict[key]){
            if (this.jaimepas_dict[key].hasOwnProperty(elm)){
              dict['dic'][key].value.push(elm);
            }
          }
        }
      }
    }

    // Envoi de la requete
    if(dict) {
      this.dataService.addPref(this.user_id, dict).subscribe((response) => {
        if (response.hasOwnProperty('error')) {
          console.log('Impossible d\'envoyer preff. error : ', response['error']);
        }
        else {
          this.jaimepas_dict = {};
          this.parent.jaimepas_show = false;

          this.parent.getBest();
        }
      }, (error) => {
        console.log('Impossible d\'envoyer preff. error : ' + error);
      });
    }
  }

  addJaimepas(key, key2){
    // initialisation si besoin
    if (!this.jaimepas_dict.hasOwnProperty(key)){
      this.jaimepas_dict[key] = {};
    }

    // cas suppression
    if (this.jaimepas_dict[key].hasOwnProperty(key2)){
      delete this.jaimepas_dict[key][key2];
    }
    // cas ajout
    else {
      this.jaimepas_dict[key][key2] = '';
    }

    //suppression des tableaux vides
    if ((key === 'style' || key === 'symbolique') && this.isEmpty(this.jaimepas_dict[key])){
      delete this.jaimepas_dict[key]
    }
  }

  isSelected(key, key2){
    if (this.jaimepas_dict.hasOwnProperty(key)){
      return this.jaimepas_dict[key].hasOwnProperty(key2);
    }

    return false;
  }

  isEmpty(obj){
    for(let prop in obj) {
      if(obj.hasOwnProperty(prop))
        return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
  }

}
