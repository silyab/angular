import {Pipe} from "@angular/core";

@Pipe({
  name: 'truncate'
})
export class TruncatePipe {
  transform(value: string, max_length: number) : string {
    if (!value) return '';
    return value.length > max_length ? value.substring(0, max_length-3) + '...' : value;
  }
}

@Pipe({
  name: 'removeBrand'
})
export class RemoveBrandPipe {
  transform(value: string, brand: string) : string {
    return value.substring(0, brand.length) === brand ? value.substring(brand.length+1, value.length) : value;
  }
}
