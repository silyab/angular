import {style, keyframes, animate, transition, state, trigger} from "@angular/core";
export let animations = [
  /* Fly droite puis droite */
  trigger('flyFromRight', [
    state('in', style({})),
    transition('void => *', [
      animate(300, keyframes([
        style({opacity: 0, transform: 'translateX(100%)', offset: 0}),
        style({opacity: 1, transform: 'translateX(-15px)',  offset: 0.3}),
        style({opacity: 1, transform: 'translateX(0)',     offset: 1.0})
      ]))
    ]),
    transition('* => void', [
      animate(300, keyframes([
        style({opacity: 1, transform: 'translateX(0)',     offset: 0}),
        style({opacity: 1, transform: 'translateX(-15px)', offset: 0.7}),
        style({opacity: 0, transform: 'translateX(100%)',  offset: 1.0})
      ]))
    ])
  ]),

  /* Fly droite puis gauche */
  trigger('flyRightLeft', [
    state('left', style({transform: 'translateX(-100%)', opacity: 0})),
    state('middle', style({transform: 'translateX(0)'})),
    state('right', style({transform: 'translateX(100%)', opacity: 0})),
    transition('* => *', animate('400ms ease')),
  ]),

  /* Fly bas */
  trigger('flyIn', [
    state('in', style({})),
    transition('void => *', [
      animate(300, keyframes([
        style({opacity: 0, transform: 'translateY(100%)', offset: 0}),
        style({opacity: 1, transform: 'translateY(-15px)',  offset: 0.3}),
        style({opacity: 1, transform: 'translateY(0)',     offset: 1.0})
      ]))
    ])
  ]),

  /* Fly haut */
  trigger('flyInOut', [
    state('in', style({})),
    transition('void => *', [
      animate(200, keyframes([
        style({opacity: 0, transform: 'translateY(-100%)', offset: 0}),
        style({opacity: 1, transform: 'translateY(-15px)',  offset: 0.4}),
        style({opacity: 1, transform: 'translateY(0)',     offset: 1.0})
      ]))
    ]),
    transition('* => void', [
      animate(300, keyframes([
        style({opacity: 1, transform: 'translateY(0)', offset: 0}),
        style({opacity: 1, transform: 'translateY(15px)',  offset: 0.3}),
        style({opacity: 1, transform: 'translateY(-100%)',     offset: 1.0})
      ]))
    ])
  ]),

  /* Rotate */
  trigger('rotate', [
      state('in', style({})),
    transition('void => *', [
      animate(400, keyframes([
        style({opacity: 0, transform: 'rotateY(90deg)',  offset: 0}),
        style({opacity: 0.3, transform: 'rotateY(0deg)', offset: 0.2}),
        style({opacity: 0.6, transform: 'rotateY(90deg)',  offset: 0.5}),
        style({opacity: 1, transform: 'rotateY(0deg)',   offset: 1.0})
      ]))
    ]),
    transition('* => void', [
      animate(400, keyframes([
        style({opacity: 1, transform: 'rotateY(90deg)',  offset: 0}),
        style({opacity: 0.6, transform: 'rotateY(0deg)', offset: 0.2}),
        style({opacity: 0.3, transform: 'rotateY(90deg)',  offset: 0.5}),
        style({opacity: 0, transform: 'rotateY(0deg)',   offset: 1.0})
      ]))
    ])
  ]),

  /* Fade */
  trigger("fade", [
    state("in", style({opacity: 1})),
    state("void", style({opacity: 0})),
    transition("in <=> *", animate( "400ms ease")),
  ]),

  /* fadeJustIn */
  trigger("fadeJustIn", [
    state("in", style({opacity: 1})),
    state("void", style({opacity: 0})),
    transition("* => in", animate( "400ms ease")),
  ]),

  /* Fade Delay */
  trigger("fadeDelay", [
    state("in", style({opacity: 1})),
    state("void", style({opacity: 0, display: 'none'})),
    transition("* => in", animate( "200ms ease")),
    transition("in => *", animate( "0ms ease")),
  ]),
];
