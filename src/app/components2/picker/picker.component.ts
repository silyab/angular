import {Component, Input, OnInit} from '@angular/core';
import {animations} from "../../styles/animations";

@Component({
  selector: 'app-picker',
  templateUrl: './picker.component.html',
  styleUrls: ['./picker.component.css', '../selector/selector.component.css', '../../styles/Xlarge.css',
    '../../styles/large.css', '../../styles/medium.css', '../../styles/tablet.css', '../../styles/phone.css'],
  animations: animations
})
export class PickerComponent implements OnInit {

  @Input('parent') parent;
  @Input('data_key') key;
  selected = [];

  data = {
  'style': {'name': 'un style', 'pluriel': 'Tous les styles', 'data':[]},
  'aspect': {'name': 'un aspect', 'pluriel': 'Tous les aspects', 'data':[]},
  'couleur_metal': {'name': 'une couleur', 'pluriel': 'Toutes les couleurs', 'data':[{'name': 'Couleur', 'styles': [
      {'name': 'Dorée', 'value': 'Dore', 'color': '#fff207'},
      {'name': 'Argentée', 'value': 'Argente', 'color': '#D7D6D2'},
      {'name': 'Rose', 'value': 'Rose', 'color': '#FFD6BA'},
      {'name': 'Noir', 'value': 'Noir', 'color': '#100F0D'},]},
    ]},
  'collier_longueur': {'name': 'longueur collier', 'pluriel': 'Toutes les tailles', 'data':[{'name': 'Longueurs', 'styles': [
      {'name': 'Ras du cou', 'value': 'Ras_du_cou'},
      {'name': 'Poitrine', 'value': 'Poitrine'},
      {'name': 'Décolleté', 'value': 'Decolte'},
      {'name': 'Sautoir', 'value': 'Sautoir'},]},
    ]},
  'montre_bracelet': {'name': 'une matière', 'pluriel': 'Toutes les matières', 'data':[{'name': 'Matières', 'styles': [
      {'name': 'Cuir', 'value': 'Cuir'},
      {'name': 'Metal', 'value': 'Metal'},
      {'name': 'Caoutchouc', 'value': 'Caoutchouc'},]},
    ]},
  'couleur': {'name': 'une couleur', 'pluriel': 'Toutes les couleurs', 'data':[{'name': 'Couleur', 'styles': [
      {'name': 'Transparent', 'value': 'Transparent', 'color': '#fcfcfc'},
      {'name': 'Noir', 'value': 'Noir', 'color': '#000'},
      {'name': 'Blanc', 'value': 'Blanc', 'color': '#F4F4F4'},
      {'name': 'Gris', 'value': 'Gris', 'color': '#B2B2B2'},
      {'name': 'Beige', 'value': 'Beige', 'color': '#F4D7C3'},
      {'name': 'Marron', 'value': 'Marron', 'color': '#522D27'},
      {'name': 'Marron clair', 'value': 'Marron_clair', 'color': '#AB7954'},
      {'name': 'Rouge', 'value': 'Rouge', 'color': '#E20E23'},
      {'name': 'Rouge bordeau', 'value': 'Rouge_borde', 'color': '#AA1B29'},
      {'name': 'Rose', 'value': 'Rose', 'color': '#e84763'},
      {'name': 'Rose clair','value':'Rose_clair', 'color': '#F8D8DD'},
      {'name': 'Bleu', 'value': 'Bleu', 'color': '#2D49AD'},
      {'name': 'Bleu clair', 'value': 'Bleu_clair', 'color': '#94E1F3'},
      {'name': 'Bleu-Vert', 'value': 'Bleu_vert', 'color': '#25596E'},
      {'name': 'Violet', 'value': 'Violet', 'color': '#623D8B'},
      {'name': 'Vert', 'value': 'Vert', 'color': '#006F44'},
      {'name': 'Orange', 'value': 'Orange', 'color': '#FF8B02'},
      {'name': 'Jaune', 'value': 'Jaune', 'color': '#FFDC0E'},]},
    ]},
};

  data_style = {
    'Bague': {'name': 'un style', 'pluriel': 'Tous les styles', 'data':[{'name': 'Styles', 'styles': [
      {'name': 'Moderne', 'value': 'moderne'},
      {'name': 'Simple', 'value': 'simple'},
      {'name': 'Classique', 'value': 'classique'},
      {'name': 'Antique', 'value': 'antique'},
      {'name': 'Rock', 'value': 'rock'},
      {'name': 'Enfantin', 'value': 'petite_fille'},]}
    ]},
    'Collier': {'name': 'un style', 'pluriel': 'Tous les styles', 'data':[{'name': 'Styles', 'styles': [
      {'name': 'Moderne', 'value': 'moderne'},
      {'name': 'Simple', 'value': 'simple'},
      {'name': 'Classique', 'value': 'classique'},
      {'name': 'Antique', 'value': 'antique'},
      {'name': 'Rock', 'value': 'rock'},
      {'name': 'Enfantin', 'value': 'petite_fille'},]}
    ]},
    'Boucle_oreil': {'name': 'un style', 'pluriel': 'Tous les styles', 'data':[{'name': 'Styles', 'styles': [
      {'name': 'Moderne', 'value': 'moderne'},
      {'name': 'Simple', 'value': 'simple'},
      {'name': 'Classique', 'value': 'classique'},
      {'name': 'Antique', 'value': 'antique'},
      {'name': 'Ancien', 'value': 'ancien'},
      {'name': 'Rock', 'value': 'rock'},
      {'name': 'Enfantin', 'value': 'petite_fille'},]}
    ]},
    'Bracelet': {'name': 'un style', 'pluriel': 'Tous les styles', 'data':[{'name': 'Styles', 'styles': [
      {'name': 'Moderne', 'value': 'moderne'},
      {'name': 'Simple', 'value': 'simple'},
      {'name': 'Classique', 'value': 'classique'},
      {'name': 'Antique', 'value': 'antique'},
      {'name': 'Rock', 'value': 'rock'},
      {'name': 'Enfantin', 'value': 'petite_fille'},]}
    ]},
  };

  data_forme = {
    'Bague': {
      'name': 'une forme', 'pluriel': 'Toutes les formes', 'data': [{
        'name': 'Forme', 'styles': [
          {'name': 'Bombé', 'value': 'bombe'},
          {'name': 'Courbures', 'value': 'courbure'},
          {'name': 'Carré', 'value': 'carre'},
          {'name': 'Feuillu', 'value': 'feuillu'},
          {'name': 'Epineux', 'value': 'epineux'},
          {'name': 'Structuré', 'value': 'structure'},
          {'name': 'Enchevêtré', 'value': 'enchevetre'},]
      }]
    },
    'Collier': {
      'name': 'une forme', 'pluriel': 'Toutes les formes', 'data': [{
        'name': 'Forme', 'styles': [
          {'name': 'Bombé', 'value': 'bombe'},
          {'name': 'Courbures', 'value': 'courbure'},
          {'name': 'Carré', 'value': 'carre'},
          {'name': 'Structuré', 'value': 'structure'},
          {'name': 'Enchevêtré', 'value': 'enchevetre'},]
      }]
    },
    'Boucle_oreil': {
      'name': 'une forme', 'pluriel': 'Toutes les formes', 'data': [{
        'name': 'Forme', 'styles': [
          {'name': 'Bombé', 'value': 'bombe'},
          {'name': 'Courbures', 'value': 'courbure'},
          {'name': 'Carré', 'value': 'carre'},
          {'name': 'Epineux', 'value': 'epineux'},
          {'name': 'Feuillu', 'value': 'feuillu'},
          {'name': 'Enchevêtré', 'value': 'enchevetre'},]
      }]
    },
    'Bracelet': {
      'name': 'une forme', 'pluriel': 'Toutes les formes', 'data': [{
        'name': 'Forme', 'styles': [
          {'name': 'Bombé', 'value': 'bombe'},
          {'name': 'Courbures', 'value': 'courbure'},
          {'name': 'Carré', 'value': 'carre'},
          {'name': 'Structuré', 'value': 'structure'},
          {'name': 'Enchevêtré', 'value': 'enchevetre'},]
      }]
    },
  };

  data_aspect = {
    'Bague': {'name': 'un aspect', 'pluriel': 'Tous les aspects', 'data':[
      {'name': 'Aspect général', 'styles': [
        {'name': 'Lisse', 'value': 'lisse', 'friends': ['brillant']},
        {'name': 'Granuleux', 'value': 'texture', 'friends': ['granuleux']}]},
      {'name': 'Type de pierre', 'styles': [
        {'name': 'Petites pierres', 'value': 'bcoup_petit_diam', 'friends': ['petit_diam_epar', 'rail_diam']},
        {'name': 'Grosse pierre', 'value': 'pierre_taille', 'friends': ['pierre_lisse']},
        {'name': 'Pierre sertie', 'value': 'pierre_sertie'}]},
      {'name': 'Divers', 'styles': [
        {'name': 'Bague articulée', 'value': 'articulee'},
        {'name': 'Mélange de bijoux', 'value': 'deux_bijoux'},
        {'name': 'Multicolore', 'value': 'multicolor', 'friends': ['heteroclite']},
        {'name': 'Avec des chaînes', 'value': 'chaine'}]},
    ]},
    'Collier': {'name': 'un aspect', 'pluriel': 'Tous les aspects', 'data':[
      {'name': 'Aspect général', 'styles': [
        {'name': 'Lisse', 'value': 'lisse'},
        {'name': 'Granuleux', 'value': 'texture', 'friends': ['granuleux', 'gravure']},
        {'name': 'Collier chaîne', 'value': 'chaine'},
        {'name': 'Collier fil', 'value': 'fil'}]},
      {'name': 'Type de pierre', 'styles': [
        {'name': 'Petites pierres', 'value': 'bcoup_petit_diam', 'friends': ['petit_diam_epar']},
        {'name': 'Pierre sertie', 'value': 'pierre_taille', 'friends': ['pierre_lisse', 'pierre_sertie']}]},
      {'name': 'Divers', 'styles': [
        {'name': 'Mélange de bijoux', 'value': 'deux_bijoux'},
        {'name': 'Multicolore', 'value': 'multicolor', 'friends': ['heteroclite']}]},
    ]},
    'Boucle_oreil': {'name': 'un aspect', 'pluriel': 'Tous les aspects', 'data':[
      {'name': 'Aspect général', 'styles': [
        {'name': 'Lisse', 'value': 'lisse', 'friends': ['brillant']},
        {'name': 'Granuleux', 'value': 'texture', 'friends': ['granuleux', 'gravure']}]},
      {'name': 'Type de pierre', 'styles': [
        {'name': 'Petites pierres', 'value': 'bcoup_petit_diam', 'friends': ['petit_diam_epar', 'rail']},
        {'name': 'Pierre sertie', 'value': 'pierre_taille', 'friends': ['pierre_lisse', 'pierre_sertie']}]},
      {'name': 'Divers', 'styles': [
        {'name': 'Avec des chaînes', 'value': 'chaine'},
        {'name': 'Mélange de bijoux', 'value': 'deux_bijoux'},
        {'name': 'Multicolore', 'value': 'multicolor', 'friends': ['heteroclite']}]},
    ]},
    'Bracelet': {'name': 'un aspect', 'pluriel': 'Tous les aspects', 'data':[
      {'name': 'Aspect général', 'styles': [
        {'name': 'Lisse', 'value': 'lisse', 'friends': ['brillant']},
        {'name': 'Granuleux', 'value': 'texture', 'friends': ['granuleux', 'gravure']},
        {'name': 'Bracelet chaîne', 'value': 'chaine'},
        {'name': 'Bracelet fil', 'value': 'fil'}]},
      {'name': 'Type de pierre', 'styles': [
        {'name': 'Petites pierres', 'value': 'bcoup_petit_diam', 'friends': ['petit_diam_epar', 'rail']},
        {'name': 'Pierre sertie', 'value': 'pierre_taille', 'friends': ['pierre_lisse', 'pierre_sertie']}]},
      {'name': 'Divers', 'styles': [
        {'name': 'Mélange de bijoux', 'value': 'deux_bijoux'},
        {'name': 'Multicolore', 'value': 'multicolor', 'friends': ['heteroclite']}]},
    ]},
  };


  constructor() {
  }

  ngOnInit() {
    if (this.data_style.hasOwnProperty(this.parent.type)){
      this.data['style'] = this.data_style[this.parent.type];
    }
    if (this.data_aspect.hasOwnProperty(this.parent.type)){
      this.data['aspect'] = this.data_aspect[this.parent.type];
    }
    if (this.data_forme.hasOwnProperty(this.parent.type)){
      this.data['forme'] = this.data_forme[this.parent.type];
    }
  }

  initState(){
    this.selected = [];
  }

  addValue(style){
    this.parent.changed[this.key] = true;
    this.parent.changed_glob = true;

    for (let elm of this.selected){
      if (elm.value === style.value) return;
    }
    style['check'] = true;
    this.selected.push(style);
    this.parent.filter[this.key][style.value] = '';
    if (style.hasOwnProperty('friends')){
      for (let friend of style.friends){
        this.parent.filter[this.key][friend] = '';
      }
    }
  }

  changeValue(elm){
    this.parent.changed[this.key] = true;
    this.parent.changed_glob = true;
    elm.check = !elm.check;

    if (elm.value in this.parent.filter[this.key]) {
      delete this.parent.filter[this.key][elm.value];
      if (elm.hasOwnProperty('friends')){
        for (let friend of elm.friends){
          delete this.parent.filter[this.key][friend];
        }
      }
    }
    else {
      this.parent.filter[this.key][elm.value] = '';
      if (elm.hasOwnProperty('friends')){
        for (let friend of elm.friends){
          this.parent.filter[this.key][friend] = '';
        }
      }
    }
  }

  removeAll(){
    this.parent.changed[this.key] = true;
    this.parent.changed_glob = true;
    this.selected = [];
    this.parent.filter[this.key] = {};
  }
}
