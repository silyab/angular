import {Component, Input, OnInit} from '@angular/core';
import {animations} from "../../styles/animations";

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.css', '../../styles/Xlarge.css', '../../styles/large.css',
    '../../styles/medium.css', '../../styles/tablet.css', '../../styles/phone.css'],
  animations: animations
})
export class SelectorComponent implements OnInit {

  @Input('parent') parent;
  @Input('data_key') key;
  @Input('mode') mode;
  states;
  names;

  constructor() {
  }

  ngOnInit() {

    let data = {
      'prix': ['0-200€', '200-500€', '500€ +'],
      'occasion': ['Travail', 'Quotidien', 'Soirée'],
      'taille': ['Discret', 'Normal', 'Imposant'],
      'bague_monture': ['Anneau', 'Monture basse', 'Monture haute'],
      'collier_forme': ['Rond', 'en V', 'Autre'],
      'boucle_forme': ['Rond', 'Carré', 'Droit'],
      'montre_forme': ['Rond', 'Carré', 'Autre'],
    };

    this.initState();
    this.names = [data[this.key][0], data[this.key][1], data[this.key][2]];
  }

  initState(){
    // mode speciale pour shop
    if (this.mode){
      this.states = [false, false, false];
      for (let i = 0; i<3; i++){
        if (i in this.parent.conf.filtre_prix){
          this.states[i] = true;
        }
      }
      return
    }
    this.states = [true, true, true];
  }

  changeState(i) {
    // mode speciale pour shop
    if (this.mode){

      this.states[i] = !this.states[i];

      let old_filtre = JSON.parse(JSON.stringify(this.parent.conf.filtre_prix));
      console.log(old_filtre);
      if (i in old_filtre) {
        delete old_filtre[i];
      }
      else {
        old_filtre[i] = '';
      }

      let count = 0;

      for(let prop in old_filtre) {
        if(old_filtre.hasOwnProperty(prop))
          ++count;
      }

      if (count === 0) {
        let _this = this;
        setTimeout(function(){
          _this.states[i] = !_this.states[i];}, 200);
      }
      else {
        this.parent.saveChangeConf('filtre_prix', old_filtre);
        this.parent.saveConf(6, true);
      }

      return
    }


    this.parent.changed[this.key] = true;
    this.parent.changed_glob = true;
    this.states[i] = !this.states[i];
    if (i in this.parent.filter[this.key]) {
      delete this.parent.filter[this.key][i];
    }
    else {
      this.parent.filter[this.key][i] = '';
    }

    let count = 0;

    for(let prop in this.parent.filter[this.key]) {
      if(this.parent.filter[this.key].hasOwnProperty(prop))
        ++count;
    }

    if (count === 0) {
      let _this = this;
      setTimeout(function(){
        _this.changeState(i);}, 200);

    }
  }

}
