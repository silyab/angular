import {Component, HostListener, OnInit, ViewChildren} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {DataService} from "../../services/data.service";
import {animations} from "../../styles/animations";
import {SelectorComponent} from "../selector/selector.component";
import {PickerComponent} from "../picker/picker.component";
import {ActivatedRoute, Params} from "@angular/router";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.css', '../../styles/Xlarge.css', '../../styles/large.css',
    '../../styles/medium.css', '../../styles/tablet.css', '../../styles/phone.css'],
  animations: animations
})
export class Home2Component implements OnInit {

  id;
  type;
  bijoux = [[]];
  bijoux_nbr;
  page = 0;
  bijoux_par_page = 20;
  loading = true;
  ratio;
  hover_ratio = [];
  nbr_case;
  filter;
  changed;
  changed_glob;
  first_time = true;
  show_popupin = false;

  @ViewChildren(SelectorComponent) selectorComponents;
  @ViewChildren(PickerComponent) pickerComponents;

  constructor(private auth: AuthService, private dataService: DataService, private activatedRoute: ActivatedRoute) {
    for (let i = 0; i<this.bijoux_par_page; i++){
      this.hover_ratio.push(1);
    }
  }

  ngOnInit() {
    localStorage.setItem('switch', '1');

    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['type'] === 'bague') {
        this.type = 'Bague';
      }
      else if (params['type'] === 'collier') {
        this.type = 'Collier';
      }
      else if (params['type'] === 'boucle_doreille') {
        this.type = 'Boucle_oreil';
      }
      else if (params['type'] === 'bracelet') {
        this.type = 'Bracelet';
      }
      else if (params['type'] === 'montre') {
        this.type = 'Montre';
      }
      else {
        this.type = 'Bague';
      }
    });
    this.initFilter();
    this.getMyId();
    this.resize(window.innerWidth);

    if (localStorage.getItem('popin') !== '0'){

      let _thiis = this;
      setTimeout(function(){

        _thiis.show_popupin = true;
        localStorage.setItem('popin', '0');

      }, 3000);
    }
  }

  initFilter(){
    this.filter = {'type': this.type, 'order': 'pop'};

    this.changed = {};
    this.changed_glob = false;

    let key_list = ['prix', 'occasion', 'taille', 'bague_monture', 'collier_forme', 'boucle_forme', 'montre_forme'];
    let key_list2 = ['style', 'forme', 'aspect', 'symbolique', 'couleur_metal', 'couleur', 'collier_longueur', 'montre_bracelet'];

    for (let key of key_list){
      this.filter[key] = {0:'', 1:'', 2:''};
      this.changed[key] = false;
    }

    for (let key of key_list2){
      this.filter[key] = {};
      this.changed[key] = false;
    }

    if(this.selectorComponents !== undefined){
      for (let selector of this.selectorComponents._results){
        selector.initState();
      }
    }

    if(this.pickerComponents !== undefined){
      for (let picker of this.pickerComponents._results){
        picker.initState();
      }
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resize(event.target.innerWidth);
  }

  resize(width) {
    if (width >= 1500) {
      this.ratio = 0.6;
    }
    else if (width >= 1200) {
      this.ratio = 0.45;
    }
    else if (width >= 992) {
      this.ratio = 0.45
    }
    else if (width >= 768) {
      this.ratio = 0.45
    }
    else {
      this.ratio = 0.45
    }


    if (width >= 2134) {
      this.nbr_case = 6;
    }
    else if (width >= 1789) {
      this.nbr_case = 5;
    }
    else if (width >= 1214) {
      this.nbr_case = 4;
    }
    else if (width >= 927) {
      this.nbr_case = 3;
    }
    else if (width >= 640) {
      this.nbr_case = 2;
    }
    else {
      this.nbr_case = 1;
    }
  }

  getMyId(){
    this.id = localStorage.getItem('clientID');

    /* Nouvel id si pas d'id*/
    if (this.id === null){
      this.askNewId();
    }
    /* Test si l'id toujours valable */
    else {
      this.dataService.testId(this.id).subscribe((response) => {
        if (response.hasOwnProperty('error')) {
          /* Si not found on demande un nouvel id */
          if (response.error === 'not found'){
            console.log('Id plus valable demande d\'un nouveau...');
            this.askNewId();
          }
          /* autre erreur */
          else {
            console.log('Le serveur a rencontré une erreur avec mon id');
            this.monitorError();
          }
        }
        else {
          this.getFiltered();
        }
      }, (error) => {
        console.log('Impossible de contacter le serveur : ' + error);
        this.monitorError();
      });
    }
  }

  askNewId(){
    this.dataService.getId().subscribe((response) => {
      this.id = response;
      this.dataService.createUser(this.id).subscribe((response) => {
        if(response.hasOwnProperty('error')){
          console.log('Impossible de créer un nouvel utilisateur, error : ', response['error']);
          this.monitorError();
        }
        else{
          localStorage.setItem('clientID', this.id);
          this.getFiltered();
        }
      }, (error) => {
        console.log('Impossible de créer un nouvel utilisateur, error : ', error);
        this.monitorError();
      });
    }, (error) => {
      console.log('Impossible de créer un nouvel utilisateur, error : ', error);
      this.monitorError();
    });
  }

  getFiltered() {
    this.loading = true;
    for (let elm in this.changed){
      if (this.changed.hasOwnProperty(elm)){
        this.changed[elm] = false;
      }
    }
    this.changed_glob = false;

    this.dataService.getFiltered(this.id, this.filter).subscribe((response) => {
      if (response.hasOwnProperty('error')) {
        console.log('Impossible de récupérer la sélection. error : ', response['error']);
        this.loading = false;
        this.monitorError();
      }
      else {
        this.page = 0;
        this.bijoux = [[]];
        let i = 0;
        let j = -1;
        for (let bijou of response){
          if (i%this.bijoux_par_page === 0){
            j++;
            this.bijoux[j] = [];
          }
          this.bijoux[j].push(bijou);
          i++;
        }
        this.bijoux_nbr = i;
        this.loading = false;
        this.first_time = false;
      }
    }, (error) => {
      console.log('Impossible de récupérer les best bijoux. error : ' + error);
      this.loading = false;
      this.monitorError();
    });
  }

  changeOrder(key) {
    if (key === 'pop'){
      if (this.filter.order !== 'pop'){
        this.filter.order = 'pop';
        this.getFiltered();
      }
    }
    else if (key === 'prix'){
      if (this.filter.order === 'pop'){
        this.filter.order = 'prix_asc';
      }
      else if (this.filter.order === 'prix_asc'){
        this.filter.order = 'prix_des';
      }
      else if (this.filter.order === 'prix_des'){
        this.filter.order = 'prix_asc';
      }

      this.getFiltered();
    }
  }

  monitorBijou(bijou_id){
    if (environment.production) {
      eventFire(document.getElementById('pixel2'), 'click');
      eventFire(document.getElementById('google2'), 'click');
    }
    this.dataService.monitorBijou(this.id, 1, bijou_id).subscribe((response) => {
    }, (error) => {
      console.log('Impossible de log bijou. error : ' + error);
    });
  }

  monitorError(){
    if (environment.production) {
      eventFire(document.getElementById('pixel3'), 'click');
    }
  }

  goTop(){
    eventFire(document.getElementById('scroll'), 'click');
  }
}



function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    let evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}
