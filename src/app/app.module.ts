import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import { MnFullpageDirective, MnFullpageService } from "ng2-fullpage";

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import {Home2Component} from "./components2/home2/home2.component";
import { StyleComponent } from './components/style/style.component';
import {AUTH_PROVIDERS} from "angular2-jwt";
import {AuthService} from "./services/auth.service";
import {DataService} from "./services/data.service";
import { ShopComponent } from './components/shop/shop.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { SelectorComponent } from './components2/selector/selector.component';
import {TruncatePipe, RemoveBrandPipe} from "./styles/pipes";
import { PickerComponent } from './components2/picker/picker.component';
import {WindowRefService} from "./services/window.service";
import { SwitcherComponent } from './components/switcher/switcher.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Home2Component,
    MnFullpageDirective,
    StyleComponent,
    ShopComponent,
    FooterComponent,
    HeaderComponent,
    SelectorComponent,
    TruncatePipe,
    RemoveBrandPipe,
    PickerComponent,
    SwitcherComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
  ],
  providers: [
    MnFullpageService,
    AuthService,
    AUTH_PROVIDERS,
    DataService,
    WindowRefService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
