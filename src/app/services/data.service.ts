import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService{

  ip = environment.ip;

  basic_header = new Headers();

  constructor(private _http: Http){
    this.basic_header.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
    this.basic_header.append('dataType-Type', 'json');
  }

  createUser(id){
    let url = 'http://'+this.ip+'/user/'+id;

    return this._http
      .put(url, '', {headers: this.basic_header})
      .map(res => res.json());
  }

  getId(){
    let url = 'http://'+this.ip+'/user_id';

    return this._http
      .get(url)
      .map(res => res.json());
  }

  testId(id){
    let url = 'http://'+this.ip+'/user/'+id;

    return this._http
      .get(url)
      .map(res => res.json());
  }

  setConf(id, conf){
    let url = 'http://'+this.ip+'/user/'+id+'/conf';

    return this._http
      .put(url, 'data=' + JSON.stringify(conf), {headers: this.basic_header})
      .map(res => res.json());
  }

  getBest(id, n){
    let url = 'http://'+this.ip+'/user/'+id+'/best/'+n;

    return this._http
      .get(url)
      .map(res => res.json());
  }

  getFiltered(id, filter){
    let url = 'http://'+this.ip+'/user/'+id+'/filter';

    return this._http
      .put(url, 'data=' + JSON.stringify(filter), {headers: this.basic_header})
      .map(res => res.json());
  }

  addPref(id, dict){
    let url = 'http://'+this.ip+'/user/'+id+'/pref';

    return this._http
      .put(url, 'data=' + JSON.stringify(dict), {headers: this.basic_header})
      .map(res => res.json());
  }

  resetPref(id){
    let url = 'http://'+this.ip+'/user/'+id+'/pref/reset';

    return this._http
      .get(url)
      .map(res => res.json());
  }

  monitorBijou(id, mode, bijou_id){
    let url = 'http://'+this.ip+'/user/'+id+'/'+mode+'/monitor/bijou/'+bijou_id;

    return this._http
      .get(url)
      .map(res => res.json());
  }
}
