import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {HomeComponent} from "./components/home/home.component";
import {Home2Component} from "./components2/home2/home2.component";
import {SwitcherComponent} from "./components/switcher/switcher.component";

const appRoutes: Routes = [
  {
    path:'',
    component: SwitcherComponent,
    pathMatch: 'full'
  },
  {
    path:'conseil',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path:'bijoux/:type',
    component: Home2Component,
    pathMatch: 'full'
  },
  {
    path:'bijoux',
    component: Home2Component,
    pathMatch: 'full'
  },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
